package com.example.respuit.proyecto1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText mEditText;
    private EditText mPassword;
    private String username;
    private String password;
    private Button log;
    //public static ArrayList<User> users = new ArrayList<>();
    public credentialsSaveAndCheck credentialsSaveCheck = new credentialsSaveAndCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = findViewById(R.id.signin1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
       /* User admin = new User("admin", "1234");
        User bdmin = new User("bdmin", "12345");
        User cdmin = new User("cdmin", "123456");
        users.add(admin);
        users.add(bdmin);
        users.add(cdmin);*/

    }
//Check credentials if User exits on credentialsSaveAndCheck( class created for easy data transfer in between activities).
    public void check(View view){
        mPassword = findViewById(R.id.pass);
        mEditText = findViewById(R.id.user);
        mPassword.setError(null);
        username = mEditText.getText().toString();
        password = mPassword.getText().toString();

        if(!credentialsSaveCheck.userCredentialCheck(username, password)){
            showError();
        }else{
            Intent intent = new Intent(this, RvActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        }
    }

//Only error.
    private void showError(){
        mPassword.setError("Password and username didn't match");
    }

    /*private boolean userCredentialCheck(){
        for(int i = 0 ; i<users.size(); i++){
            if(users.get(i).getUsername().equals(username) && users.get(i).getPassword().equals(password)){
                Log.d("USERNAME++++++", "CORRECTO");

                //Launch next activity ++++++++++++++++++++++++++++++++++++++++++++

                return true;
            }
        }
        return false;
    }*/


    //hi


//Launch next activity and pass it a credentialsSaveAndCheck. (Note the "startActivityForResult" to get back a possible modification of credentialSaveCheck1 ).
    public void signIn(){
        Intent signInPage = new Intent(this, Signin.class);
        credentialsSaveAndCheck credentialsSaveCheck1 = new credentialsSaveAndCheck();
        signInPage.putExtra("usersSaved", credentialsSaveCheck1);
        startActivityForResult(signInPage,1);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


    }
//Fetch the information passed from the child activity.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            if (requestCode == 1 && resultCode == RESULT_OK) {
                //Intent in = getIntent();
                Bundle in = data.getExtras();
                credentialsSaveAndCheck csaM = (credentialsSaveAndCheck) in.getSerializable("u");

                credentialsSaveCheck = csaM;
                Log.d("OBJECTPASS", " " +  credentialsSaveCheck.toString());

            }
        }
    }
}
