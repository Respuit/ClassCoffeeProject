package com.example.respuit.proyecto1;

import android.widget.Switch;

import java.io.Serializable;
import java.util.ArrayList;

public class CartItem implements Serializable{
    private String cartName, cartGrind;
    private int cartQuantity,price;

    public CartItem(String newName, String newGrind, String quantityS, int newPrice){
        cartName = newName;
        cartGrind = newGrind;
        cartQuantity = Integer.parseInt(quantityS);
        price = newPrice;

    }

    public String getName(){
        return cartName;
    }

    public String getGrind(){
        return cartGrind;
    }

    public int getQuantity(){
        return cartQuantity;
    }

    public int getPrice(){
        switch(cartQuantity){
            case 125:
                return price/4;
            case 250:
                return price/3;
            case 500:
                return price/2;
            case 1000:
                return price;
            default:
                return price;
        }

    }
}
