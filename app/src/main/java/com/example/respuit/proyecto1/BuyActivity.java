package com.example.respuit.proyecto1;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;

public class BuyActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, PopupMenu.OnMenuItemClickListener{

    private ArrayList<String> imageUrls = new ArrayList<>() ;

    private Coffee cof;
    private TextView name, info, price;
    private String grind = "In beans";
    private String quantity = "";
    private LottieAnimationView buy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);

        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,R.array.weight, R.layout.spinner_style);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter1);
        spinner.setOnItemSelectedListener(this);


       buy = findViewById(R.id.lottiebuy);




        Intent intent = getIntent();
        Coffee cofe =(Coffee) intent.getSerializableExtra("coffeeSelected");
        cof = cofe;
        name = findViewById(R.id.buyName);
        info = findViewById(R.id.buyInfo);
        price = findViewById( R.id.buyPrice);
        name.setText(cof.getName());
        info.setText(cof.getINfo());
        price.setText("Price per kilogram: " + cof.getPrice() + "$");
        imageUrls.add(cof.getImage());
        imageUrls.add(cof.getImage2());
        imageUrls.add(cof.getImage3());
       //i.setImageURI(Uri.parse(cof.getImage()));
        Log.d("holita", cof.getImage());

        ViewPager viewPager = findViewById(R.id.view_pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(this, imageUrls);
        viewPager.setAdapter(adapter);
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?

        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.moka:
                if (checked)
                    grind = "moka";
                    log();
                    break;
            case R.id.espresso:
                if (checked)
                    grind = "espresso";
                    log();
                    break;

            case R.id.french:
                if(checked)
                    grind = "french";
                    log();
                    break;
        }
    }


   //check radio buttons don't use (don't delete yet it may be helpful)
    public void log(){
        Log.d("RADIO", grind);
    }

    public void exit(View view){
        finish();
    }

    public void showPopUp(View view){
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_menu);
        popup.show();
    }


    //Handle the selected item on popup menu.
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(BuyActivity.this, MainActivity.class);
                //CartItem it = new CartItem(cof.getName(), grind, quantity, cof.getPrice());
                startActivity(intent);
                return true;
            case R.id.item2:
                //launch cart activity

                if(!RvActivity.items.isEmpty()){
                    Intent intent1 = new Intent(BuyActivity.this, LastActivity.class);
                    intent1.putExtra("its",RvActivity.items);
                    startActivity(intent1);
                }else{
                   Toast pass = Toast.makeText(this, "The cart is empty!", Toast.LENGTH_SHORT);
                    pass.show();
                }


                return true;
            default:
                return false;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        quantity = parent.getItemAtPosition(position).toString();
        //Toast.makeText(parent.getContext(), quantity, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    //Dialog

    public void dialog(View v){

        buy.playAnimation();

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle("Your order");
        builder.setMessage("Grind: " + grind + "\nQuantity: " + quantity);
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent= new Intent(BuyActivity.this, RvActivity.class);

                        CartItem it = new CartItem(cof.getName(), grind, quantity, cof.getPrice());
                        intent.putExtra("i",it);
                        setResult(RESULT_OK, intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
