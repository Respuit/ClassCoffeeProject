package com.example.respuit.proyecto1;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.TextViewHolder>
        implements View.OnClickListener {

    private ArrayList<Coffee> Coffees;
    private View.OnClickListener listener;

    public void setItems(ArrayList<Coffee> items){
        Coffees = items;
        //notifyDataSetChanged();
    }


    @Override
    public TextViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        view.setOnClickListener(this);
        TextViewHolder textViewHolder = new TextViewHolder(view);
        return textViewHolder;
    }

    @Override
    public void onBindViewHolder(TextViewHolder holder, int position) {
        Coffee cf = Coffees.get(position);

        

        holder.bind(cf);
    }

    @Override
    public int getItemCount() {
        return Coffees.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onClick(v);
    }


    static class TextViewHolder extends RecyclerView.ViewHolder {

        private ImageView img;
        private TextView txt;

        public TextViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.itemlistimage);
            txt = itemView.findViewById(R.id.itemlisttext);
        }

        public void bind(Coffee cf) {
            img.setImageURI(Uri.parse(cf.getImage()));
            txt.setText(cf.getName());

        }
    }
}
