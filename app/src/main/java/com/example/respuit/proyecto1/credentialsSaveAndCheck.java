package com.example.respuit.proyecto1;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

public class credentialsSaveAndCheck implements Serializable {
    private ArrayList<User> users = new ArrayList<>();

    public credentialsSaveAndCheck(){
        User defaultadmin = new User("admin", "1234");
        users.add(defaultadmin);
    }

    public boolean userCredentialCheck(String inUsername, String inPassword){
        for(int i = 0 ; i<users.size(); i++){
            if(users.get(i).getUsername().equals(inUsername) && users.get(i).getPassword().equals(inPassword)){
                Log.d("USERNAME++++++", "CORRECTO");

                //Launch next activity ++++++++++++++++++++++++++++++++++++++++++++

                return true;
            }
        }
        return false;
    }

    public void addUser(User newUser){
        users.add(newUser);
    }

    public String toString(){
        String aux = "";
        for(int i = 0; i<users.size(); i++){
            aux+= users.get(i).getUsername() + ", ";
        }
        return aux;
    }

}
