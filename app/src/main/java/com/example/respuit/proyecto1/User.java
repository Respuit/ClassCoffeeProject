package com.example.respuit.proyecto1;

import java.io.Serializable;

/**
 * Created by respuit on 10/05/18.
 */

public class User implements Serializable {
    private String userId;
    private String username;
    private String password;

    public User(String newUsername, String newPassword){
        username = newUsername;
        password = newPassword;
        userId = "id" + username.toUpperCase();
    }

    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }

    public String getUserId(){
        return userId;
    }

    public boolean checkUserCredential(String inputUser, String inputPassword){
        if(inputUser.equalsIgnoreCase(username) && inputPassword.equals(password)){
            return true;
        }else{
            return false;
        }

    }

}
