package com.example.respuit.proyecto1;

import android.graphics.drawable.Drawable;
import android.net.Uri;

import java.io.Serializable;

public class Coffee implements Serializable {
    private String name, info;
    private String uriImage, uriImage2, uriImage3;
    private int price;


    public Coffee(String newName, String newInfo, String newuriImage, String newuriImage2, String newuriImage3, int newPrice){
        name = newName;
        info = newInfo;
        uriImage = newuriImage;
        uriImage2 = newuriImage2;
        uriImage3 = newuriImage3;
        price = newPrice;
    }

    public String getName(){
        return name;
    }

    public String getImage(){
        return uriImage;
    }
    public String getImage2(){
        return uriImage2;
    }
    public String getImage3(){
        return uriImage3;
    }
    public int getPrice(){
        return price;
    }

    public String getINfo(){
        return info;
    }



}
