package com.example.respuit.proyecto1;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Signin extends AppCompatActivity {

    private TextInputLayout email;
    private TextInputLayout username;
    private TextInputLayout password;
    private TextInputLayout confirmpassword;
    private Button btn;
    private String textEmail;
    private String textUsername;
    private String textPassword;
    private String textConfirmPassword;
    private credentialsSaveAndCheck csaS = new credentialsSaveAndCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        Intent intent = getIntent();
        credentialsSaveAndCheck csa = (credentialsSaveAndCheck) intent.getSerializableExtra("usersSaved");
        Log.d("OBJECTPASS1",csa.toString());
        csaS = csa;

        }
//Just get back to login activity.
    public void exit(View view){
        finish();
    }

//Get all that have been written on the activity.
    public void addUser(View v){
        email = findViewById(R.id.email);
        username = findViewById(R.id.userCreate);
        password = findViewById(R.id.passCreate);
        confirmpassword = findViewById(R.id.compassCreate);
        textEmail = email.getEditText().getText().toString();
        textUsername = username.getEditText().getText().toString();
        textPassword = password.getEditText().getText().toString();
        textConfirmPassword = confirmpassword.getEditText().getText().toString();
        validate();

    }
//Don't discard just yet (send back information to the parent activity).

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /*  @Override
    public void onBackPressed() {
        Intent signInPageB = new Intent();
        credentialsSaveAndCheck credentialsSaveCheck2 = csaS;
        Log.d("OBJECTPASSBack",credentialsSaveCheck2.toString());
        signInPageB.putExtra("u", credentialsSaveCheck2);
        setResult(RESULT_OK, signInPageB);
        finish();
        // startActivity(signInPage);
    }*/

        /* public void validate(View view){
        String textEmail = email.getEditText().getText().toString();
        if(textEmail.contains("a")){
            showError();
        }
    }*/

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//Call for validation of all the information compiled. Need to call the validations first so they all display, then I can do the logical check. All error messages on its on method at the end.
//If everything is validated send back to the parent activity the updated credentialSaveAndCheck.
    public void validate() {
        validateEmail();
        validatePassword();
        validateBothPasswords();
        if(validateEmail() && validatePassword() && validateBothPasswords()){
            Toast pass = Toast.makeText(this, "Thnaks!", Toast.LENGTH_SHORT);
            pass.show();
            User newUser = new User(textUsername, textPassword);
            csaS.addUser(newUser);
            Intent signInPageB = new Intent();
            credentialsSaveAndCheck credentialsSaveCheck2 = csaS;
            Log.d("OBJECTPASSBack",credentialsSaveCheck2.toString());
            signInPageB.putExtra("u", credentialsSaveCheck2);
            setResult(RESULT_OK, signInPageB);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        }
    }

//ALL VALIDATIONS. ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//Check if E-mail have an @ and it's not empty.
    private boolean validateEmail(){
        boolean validate = false;
        if(textEmail.isEmpty()) {
            showError(2);
        }else{

            if (!textEmail.contains("@")) {
                showError(1);
            } else {
                validate = true;
                email.setError(null);
            }
        }
        return validate;
    }

//Check if the password is at least 4 characters long, for at least an upper case a lower case and a number.
    private boolean validatePassword() {
        boolean validate = false;
        if (textPassword.length()>=4){ //Length greater than 3.

            if (!textPassword.matches("^(.*?[A-Z]){1,}.*$")) { //At least 1 upper case.
                password.setError(null);
                showError(3);
            } else {
                if (!textPassword.matches("^(.*?[a-z]){1,}.*$")) { //At least 1 lower case.
                    password.setError(null);
                    showError(4);
                } else {
                    if(!textPassword.matches("^(.*?[0-9]){1,}.*$")) { //At least 1 numeric character.
                        password.setError(null);
                        showError(6);
                    }else {
                        validate = true;
                        password.setError(null);
                    }
                }
            }
    }else{showError(5);}
        return validate;
    }

//Check if both passwords are the same.
    private boolean validateBothPasswords(){
        boolean validate = true;
        if(!textPassword.equals(textConfirmPassword) || textConfirmPassword.isEmpty()){
            showError(7);
            validate = false;

        }
        return validate;
    }

//END OF VALIDATIONS.+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//All possible errors for the activity text.
    private void showError(int errorType){
        switch (errorType){
            case 1: email.setError("The E-mail must contain an @");
                break;
            case 2: email.setError("Can't be empty");
                break;
            case 3: password.setError("The password must contain at least one upper case");
                break;
            case 4: password.setError("The password must contain at least one lower case");
                break;
            case 5: password.setError("The password must contain at least 4 digits");
                break;
            case 6: password.setError("The password must contain at least one number");
                break;
            case 7: confirmpassword.setError("The passwords must be identical");
                break;
        }
    }
}

