package com.example.respuit.proyecto1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RvActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener{
    ArrayList<Coffee> cfs;
    public static ArrayList<CartItem> items = new ArrayList<>();
    CartItem c;
    private TextView total;
   // private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rv);

        total = findViewById(R.id.totalprice);

        //imageView = findViewById(R.id.itemlistimage);

        final RecyclerView recyclerView = (RecyclerView)findViewById(R.id.rvlist);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        RvAdapter rva = new RvAdapter();
        rva.setHasStableIds(true);
        recyclerView.setAdapter(rva);


//Recycler View uris
     //Colombia's image
        Uri col = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.colombiaimage);
        String cols = col.toString();

    //Ethiopia's image
        Uri eth = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.etiopia);
        String eths = eth.toString();

    //Indonesia's image
        Uri ind = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.indonesia);
        String inds = ind.toString();


        //check log
        Log.d("HOLA", col.toString());


//Information activity uris
    //Get the uri.
        Uri naranjo = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.naranjoimage1);
        Uri naranjo2 = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.naranjoimage2);
    //Convert to strings.
        String nar = naranjo.toString();
        String nar2 = naranjo2.toString();

    //Get the uri.
        Uri aponte = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.ingaaponte);
        Uri aponte2 = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.ingaaponte2);

    //Convert to strings.
        String ing = aponte.toString();
        String ing2 = aponte2.toString();

    //Get the uri.
        Uri geisha = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.geisha);
        Uri geisha2 = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.geisha2);
    //Convert to strings.
        String geish = geisha.toString();
        String geish2 = geisha2.toString();

    //Get the uri.
        Uri torban = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.torban);
        Uri torban2 = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.torban2);
    //Convert to strings.
        String torb = torban.toString();
        String torb2 = torban2.toString();

    //Get the uri.
        Uri java = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.java);
        Uri java2 = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.java2);
    //Convert to strings.
        String jav = java.toString();
        String jav2 = java2.toString();

    //Get the uri.
        Uri bener = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.bener);
        Uri bener2 = Uri.parse("android.resource://com.example.respuit.proyecto1/" + R.drawable.bener2);
    //Convert to strings.
        String bne = bener.toString();
        String bne2 = bener2.toString();



//Give parameters to Coffee objects.
        Coffee nc = new Coffee("El naranjo de colombia","Honey, stone fruit and pine resin fragrance. Sweet soy sauce aroma. Very juicy and well balanced with orange notes. Mellow with a sweet finish.", cols, nar, nar2,10);
        Coffee iia = new Coffee("Inga Aponte - washed", "Very sweet fragrance of berries, condensed milk and a hint of umami. Aroma of cola candy and fruit cocktail. Extremely sweet like sweet potato with phosphoric acidity, clear and dense.", cols, ing, ing2,5);
        Coffee ghs = new Coffee("Geisha Coffee", "Sweet tropical fruit fragrance like mango. Impressive floral aroma, fruity, notes of chai and red berries infusion. Very clean, flower bouquet, lively and fresh.", cols, geish, geish2,20);
        Coffee tbn = new Coffee("Torban Coffee", "Very floral, fresh, lemony and broth like Fragrance. Juicy aromas, pure rose, fruity, toast and lemon curd. Flower bouquet notes (rose and violet), silky, delicate, strong retro nasal aftertaste.", eths, torb, torb2,12);
        Coffee jva = new Coffee("Java weninggalih Coffee", "Sweet fragrance like chocolate milk and fruits. Aromas and flavors to pastry, cookie, very sweet and round.", inds, jav, jav2,15);
        Coffee ben = new Coffee("Bener Mutara Coffee", "Caramel fragrance with sutle notes to funghi porcini and orange peel. Very sweet aroma similar to Blue Batak. Hints of carrot, flavours between sweet and veggie, silky, mouth coating with a persistent aftertaste.", inds, bne, bne2,8);



        cfs = new ArrayList<>();
        cfs.add(nc);
        cfs.add(iia);
        cfs.add(ghs);
        cfs.add(tbn);
        cfs.add(jva);
        cfs.add(ben);

       /* for(int i = 0 ; i < cfs.size();i++){
            Drawable aux = cfs.get(i).getImage();
            Picasso
                    .get()
                    .load(String.valueOf(aux))
                    .transform(new CropCircleTransformation())
                    .into(imageView);
        }*/

        rva.setItems(cfs);
//OnClickListener++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        rva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Coffee aux = cfs.get(recyclerView.getChildAdapterPosition(v));
                //Toast.makeText(RvActivity.this, ""+ aux.getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(RvActivity.this, BuyActivity.class);
                intent.putExtra("coffeeSelected", aux);
                startActivityForResult(intent, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }
        });

        recyclerView.setAdapter(rva);
    }

    public void exit(View view){
        finish();
    }



    public void showPopUp(View view){
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_menu);
        popup.show();
    }


//Handle the selected item on popup menu.
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(RvActivity.this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.item2:
                //launch cart activity
                if(!items.isEmpty()){
                    Intent intent1 = new Intent(RvActivity.this, LastActivity.class);
                    intent1.putExtra("its",items);
                    startActivity(intent1);
                }else{
                    Toast pass = Toast.makeText(this, "The cart is empty!", Toast.LENGTH_SHORT);
                    pass.show();
                }


                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            if (requestCode == 1 && resultCode == RESULT_OK) {
                //Intent in = getIntent();
                Bundle in = data.getExtras();
                CartItem ci = (CartItem) in.getSerializable("i");

                c = ci;
                items.add(c);


                Log.d("OBJECTPASS", " " +  c.getName());

            }
        }
    }
}
