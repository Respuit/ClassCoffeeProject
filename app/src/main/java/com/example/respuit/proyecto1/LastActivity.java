package com.example.respuit.proyecto1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class LastActivity extends AppCompatActivity {

    ArrayList<CartItem> finalCart = new ArrayList<>();
    private ListView list;
    private TextView to;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last);




            Bundle extras = getIntent().getExtras();
            finalCart = (ArrayList<CartItem>) extras.getSerializable("its");

            list = (ListView) findViewById(R.id.listcart);
        Log.d("LIIIIIIIIIIIIIIIIST", finalCart.get(0).getName());
        to = findViewById(R.id.totalprice);
        to.setText("Total: " + totalPrice() + "$");

        AdaptadorTitulares adapter =
                new AdaptadorTitulares(this, finalCart);
        list.setAdapter(adapter);




    }

    public int totalPrice(){
        int aux = 0;

        for(int i = 0; i < finalCart.size(); i++){
            aux +=  finalCart.get(i).getPrice();
        }

        return aux;

    }

    class AdaptadorTitulares extends ArrayAdapter<CartItem> {



        public AdaptadorTitulares(Context context, ArrayList<CartItem> finalCart ) {
            super(context, R.layout.list, finalCart);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.list, null);



            TextView name = (TextView)item.findViewById(R.id.Namelist);
            name.setText(finalCart.get(position).getName());
            TextView grind = (TextView)item.findViewById(R.id.Grindlist);
            grind.setText(finalCart.get(position).getGrind());
            TextView quantity = (TextView)item.findViewById(R.id.Quantitylist);
            quantity.setText(finalCart.get(position).getQuantity() + "g");
            TextView price = (TextView)item.findViewById(R.id.Pricelist);
            price.setText(finalCart.get(position).getPrice() + "$");



            return(item);
        }
    }

    public void exit(View view){
        finish();
    }

    public void logout(View view){
        Toast pass = Toast.makeText(this, "Thnaks!", Toast.LENGTH_SHORT);
        pass.show();
        Intent intent = new Intent(LastActivity.this, MainActivity.class);
        startActivity(intent);
    }


}
